//------------------------------------|| PIP_MatrixMaths

//----------------------------------------------------||
matrix Translation(vector position)
    {
        return    set(1, 0, 0, 0,
                      0, 1, 0, 0,
                      0, 0, 1, 0,
                      position.x, position.y, position.z, 1);
    }
//----------------------------------------------------||
matrix RotateX(float pitch)
{
    return        set(1,           0,          0, 0,
                      0,  cos(pitch), sin(pitch), 0,
                      0, -sin(pitch), cos(pitch), 0,
                      0,           0,          0, 1);
}
//----------------------------------------------------||
matrix RotateY(float yaw)
{
    return        set(cos(yaw), 0, -sin(yaw), 0,
                             0, 1,         0, 0,
                      sin(yaw), 0,  cos(yaw), 0,
                             0, 0,         0, 1);
}
//----------------------------------------------------||
matrix RotateZ(float roll)
{
    return        set(cos(roll), sin(roll), 0, 0,
                     -sin(roll), cos(roll), 0, 0,
                              0,         0, 1, 0,
                              0,         0, 0, 1);
} 
//----------------------------------------------------||
matrix build_matrix(vector pos; vector4 rot)
{
    matrix3 rm = qconvert(rot);
    matrix  m  = matrix(rm);
    
    m.ax       = pos.x;
    m.ay       = pos.y;
    m.az       = pos.z;    
    
    return  m;
}
//----------------------------------------------------||
vector4 build_quaternion(matrix m)
{
    matrix3 rm = matrix3(m);
    vector4 r  = quaternion(rm);
    return  r;
    
}
//----------------------------------------------------||
vector position_from_matrix(matrix m)
{
    return set(m.ax,m.ay,m.az);
}
//----------------------------------------------------||
matrix PIP_Lookat(vector position; vector target; vector upVector)
{
        vector zaxis = normalize(target - position);
        vector xaxis = normalize(cross( (upVector - target) , zaxis));
        vector yaxis = normalize(cross( zaxis,xaxis));
        return set(  xaxis.x, xaxis.y, xaxis.z, 0,
                     yaxis.x, yaxis.y, yaxis.z, 0,
                     zaxis.x, zaxis.y, zaxis.z, 0,
                     position.x, position.y,position.z, 1);
}
//----------------------------------------------------||
vector4 target_quaternion(matrix m; vector target; vector up_vec; float bias)
{

    vector4 sq = build_quaternion(m); 

    vector p0  = position_from_matrix(m);
    vector p1  = p0 + target;
    vector up  = position_from_matrix( Translation(up_vec) * m);

    matrix tm  = PIP_Lookat(p0,p1,up);

    vector4 tq = build_quaternion(tm); // Get the new quaternion
    vector4 nq = slerp(sq, tq, bias);

    return nq;
}
//----------------------------------------------------||
